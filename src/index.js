import { createProxy } from "./proxy";
import http from "./http";


export const JsonRpcClient = (url = "/jsonrpc", headers = { 'Content-Type': 'application/json;charset=utf-8' }) => {
    let jsonrpc_id = 0
    const actionCall = (method, args) => {
        jsonrpc_id++;
        return http.post(url, {
            headers: headers,
            json: {
                "method": method,
                "params": args,
                "jsonrpc": "2.0",
                "id": jsonrpc_id,
            },
        }).then(res => res.json())
    }
    return createProxy(actionCall)
}


export default JsonRpcClient;