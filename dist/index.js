"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.JsonRpcClient = undefined;

var _proxy = require("./proxy");

var _http = require("./http");

var _http2 = _interopRequireDefault(_http);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var JsonRpcClient = exports.JsonRpcClient = function JsonRpcClient() {
    var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "/jsonrpc";
    var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { 'Content-Type': 'application/json;charset=utf-8' };

    var jsonrpc_id = 0;
    var actionCall = function actionCall(method, args) {
        jsonrpc_id++;
        return _http2.default.post(url, {
            headers: headers,
            json: {
                "method": method,
                "params": args,
                "jsonrpc": "2.0",
                "id": jsonrpc_id
            }
        }).then(function (res) {
            return res.json();
        });
    };
    return (0, _proxy.createProxy)(actionCall);
};

exports.default = JsonRpcClient;